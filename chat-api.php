<?php
/*
Plugin Name: Chat API
*/

//CREATE CHAT MESSAGES POST TYPE

if ( ! function_exists('cassc_cpt_create_message') ) {

// Register Custom Post Type
function cassc_cpt_create_message() {

  $labels = array(
    'name'                  => _x( 'Messages', 'Post Type General Name', 'cassc' ),
    'singular_name'         => _x( 'Message', 'Post Type Singular Name', 'cassc' ),
    'menu_name'             => __( 'Messages', 'cassc' ),
    'name_admin_bar'        => __( 'Messages', 'cassc' ),
    'archives'              => __( 'Item Archives', 'cassc' ),
    'attributes'            => __( 'Item Attributes', 'cassc' ),
    'parent_item_colon'     => __( 'Parent Item:', 'cassc' ),
    'all_items'             => __( 'All Items', 'cassc' ),
    'add_new_item'          => __( 'Add New Item', 'cassc' ),
    'add_new'               => __( 'Add New', 'cassc' ),
    'new_item'              => __( 'New Item', 'cassc' ),
    'edit_item'             => __( 'Edit Item', 'cassc' ),
    'update_item'           => __( 'Update Item', 'cassc' ),
    'view_item'             => __( 'View Item', 'cassc' ),
    'view_items'            => __( 'View Items', 'cassc' ),
    'search_items'          => __( 'Search Item', 'cassc' ),
    'not_found'             => __( 'Not found', 'cassc' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'cassc' ),
    'featured_image'        => __( 'Featured Image', 'cassc' ),
    'set_featured_image'    => __( 'Set featured image', 'cassc' ),
    'remove_featured_image' => __( 'Remove featured image', 'cassc' ),
    'use_featured_image'    => __( 'Use as featured image', 'cassc' ),
    'insert_into_item'      => __( 'Insert into item', 'cassc' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'cassc' ),
    'items_list'            => __( 'Items list', 'cassc' ),
    'items_list_navigation' => __( 'Items list navigation', 'cassc' ),
    'filter_items_list'     => __( 'Filter items list', 'cassc' ),
  );
  $args = array(
    'label'                 => __( 'Message', 'cassc' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'custom-fields' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'show_in_rest'          => true,
  );
  register_post_type( 'cassc_message', $args );

}
add_action( 'init', 'cassc_cpt_create_message', 0 );

}

//functions
function cmp($a, $b)
{
  return strcmp($a->unix_time, $b->unix_time);
}

//REST API

//main info
add_action( 'rest_api_init', function () {
  register_rest_route( 'wpchat/v1', '/getinfo/', array(
    'methods' => 'POST',
    'callback' => 'cassc_route_getinfo',
  ) );
} );

function cassc_route_getinfo() {
  $response = array();
  $user = wp_get_current_user();
  array_push($response, $user);
  $response_json = json_encode($response);

  return $response_json;
}

//users
add_action( 'rest_api_init', function () {
  register_rest_route( 'wpchat/v1', '/users/', array(
    'methods' => 'GET',
    'callback' => 'cassc_route_getusers',
  ) );
} );

function cassc_route_getusers() {
  $user_id = wp_get_current_user()->ID;

  $args = array(
    'blog_id'      => $GLOBALS['blog_id'],
    'role'         => '',
    'role__in'     => array(),
    'role__not_in' => array(),
    'meta_key'     => '',
    'meta_value'   => '',
    'meta_compare' => '',
    'meta_query'   => array(),
    'date_query'   => array(),
    'include'      => array(),
    'exclude'      => array($user_id),
    'orderby'      => 'login',
    'order'        => 'ASC',
    'offset'       => '',
    'search'       => '',
    'number'       => '',
    'count_total'  => false,
    'fields'       => 'all',
    'who'          => '',
  );

  $users = get_users($args);

  $users_response = [];

  foreach ($users as $key => $value) {
    $value->data->picurl = get_avatar_url($value->data->ID);
    array_push($users_response, $value->data);
  }

  $users_json = json_encode($users_response);

  return $users_json;
}

//messages
add_action( 'rest_api_init', function () {
  register_rest_route( 'wpchat/v1', '/messages/', array(
    'methods' => 'POST',
    'callback' => 'cassc_route_getmessages',
  ));
} );

function cassc_route_getmessages( $data ) {
  $talkable = $data->get_params()['talkable'];
  $user = wp_get_current_user()->ID;
  $all_messages = [];

  //Get messages written by current user
  // WP_Query arguments
  $args = array(
    'post_type'              => array( 'cassc_message' ),
    'author' => $user,
    'meta_query' => array(
      array(
        'key'     => 'chat_partner',
        'compare' => '=',
        'type'    => 'NUMERIC',
        'value'   => $talkable
      ),
    )
  );

  // The Query
  $query_current = new WP_Query( $args );

  $written_by_current = $query_current->posts;

  foreach ($written_by_current as $key => $value) {
    $value->unix_time = get_the_time('U', $value->ID);
    $value->author_status = 'current';
    $value->author_name = get_the_author_meta('display_name', (int)$value->post_author);
    array_push($all_messages, $value);
  }

  //Get messages written by talkable person
  // WP_Query arguments
  $args = array(
    'post_type'              => array( 'cassc_message' ),
    'author' => $talkable,
    'meta_query' => array(
      array(
        'key'     => 'chat_partner',
        'compare' => '=',
        'type'    => 'NUMERIC',
        'value'   => $user
      ),
    )
  );

  // The Query
  $query_talkable = new WP_Query( $args );

  $written_by_talkable = $query_talkable->posts;

  foreach ($written_by_talkable as $key => $value) {
    $value->unix_time = get_the_time('U', $value->ID);
    $value->author_status = 'other';
    $value->author_name = get_the_author_meta('display_name', (int)$talkable);
    array_push($all_messages, $value);
  }

  usort($all_messages, "cmp");

  //UPDATE INFO BY WHO MESSAGE IS SEEN

  //Get info who have seen last post of conversation.
  $last_id = end($all_messages)->ID;
  $seen_by = get_post_meta($last_id, "seen_by_users", true);

  error_log('pp');

  if (!is_array($seen_by)) {
    $seen_by = array();
  }

  //Is user who queries mesasages already seen last post, if not then mark that she has already seen posts
  if(!in_array($user, $seen_by)) {
    array_push($seen_by, $user);
    update_post_meta($last_id, "seen_by_users", $seen_by);
  }

  return json_encode($all_messages);
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wpchat/v1', '/addmessage/', array(
    'methods' => 'POST',
    'callback' => 'cassc_route_addmessage',
  ) );
} );

function cassc_route_addmessage($data) {
  $message = $data->get_body();
  $message_decoded = json_decode($message);
  $message_content = $message_decoded->content;
  $message_text = $message_content[0];
  $chat_partner = $message_content[1];
  $user = wp_get_current_user()->ID;

  // Create post object
  $my_post = array(
    'post_title'    => 'test',
    'post_content'  => $message_text,
    'post_status'   => 'publish',
    'post_author'   => $user,
    'post_type'     => 'cassc_message',
  );
   
  // Insert the post into the database
  $post_id = wp_insert_post( $my_post );

  add_post_meta($post_id, 'chat_partner', $chat_partner);
}

//Get one persons messages
add_action( 'rest_api_init', function () {
  register_rest_route( 'wpchat/v1', '/last-message-seen/', array(
    'methods' => 'POST',
    'callback' => 'cassc_route_last_message_seen',
  ) );
} );

function cassc_route_last_message_seen($data) {
  /*
    SEE IF LAST MESSAGE OF QUERIED USER AND MY CONVERSATION IS SEEN BY ME, IF IT IS NOT THEN SHOW MARK OF UNSEEN MESSAGE
  */

  $message = $data->get_body();
  $message_decoded = json_decode($message);
  $message_user = $message_decoded->user;

  $user = wp_get_current_user()->ID;
  $all_messages = [];

  $last_message_is_seen = false;

  //Get messages written by current user
  // WP_Query arguments
  $args = array(
    'post_type'              => array( 'cassc_message' ),
    'author'                 => $user,
    'posts_per_page'         => '1',
    'meta_query' => array(
      array(
        'key'     => 'chat_partner',
        'compare' => '=',
        'type'    => 'NUMERIC',
        'value'   => $message_user
      ),
    )
  );

  // The Query
  $query_current = new WP_Query( $args );

  $written_by_current = $query_current->posts;

  foreach ($written_by_current as $key => $value) {
    $value->unix_time = get_the_time('U', $value->ID);
    array_push($all_messages, $value);
  }

  //Get messages written by talkable person
  // WP_Query arguments
  $args = array(
    'post_type'              => array( 'cassc_message' ),
    'author' => $message_user,
    'meta_query' => array(
      array(
        'key'     => 'chat_partner',
        'compare' => '=',
        'type'    => 'NUMERIC',
        'value'   => $user
      ),
    )
  );

  // The Query
  $query_talkable = new WP_Query( $args );

  $written_by_talkable = $query_talkable->posts;

  foreach ($written_by_talkable as $key => $value) {
    $value->unix_time = get_the_time('U', $value->ID);
    array_push($all_messages, $value);
  }

  usort($all_messages, "cmp");

  if (sizeof($all_messages)>0) {
    $last_message = end($all_messages);
    //SEE IF LAST MESSSAGE IS SEEN BY CURRENT USER
    $last_message_seenby = get_post_meta($last_message->ID, "seen_by_users", true);
    if ($last_message_seenby) {
      if (in_array($user, $last_message_seenby)) {
        $last_message_is_seen = true;
      } else {
        $last_message_is_seen = false;
      }
    } else {
      $last_message_is_seen = false;
    }
  } else {
    $last_message_is_seen = true;
  }

  return json_encode($last_message_is_seen);
}